package id.assessment.localbusiness.domain.repository

import id.assessment.localbusiness.domain.model.DetailBusiness
import io.reactivex.Single

interface DetailBusinessRepository {
    fun getDetail(id: String): Single<DetailBusiness>
}