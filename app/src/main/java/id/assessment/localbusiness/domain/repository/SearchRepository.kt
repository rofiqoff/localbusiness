package id.assessment.localbusiness.domain.repository

import id.assessment.localbusiness.domain.model.Businesses
import io.reactivex.Single

interface SearchRepository {
    fun search(latitude: String, longitude: String, termToSearch: String): Single<Businesses>
}