package id.assessment.localbusiness.domain.usecase

import id.assessment.localbusiness.domain.model.DetailBusiness
import id.assessment.localbusiness.domain.repository.DetailBusinessRepository
import id.assessment.localbusiness.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class DetailBusinessUseCase @Inject constructor(
    private val repository: DetailBusinessRepository
) : SingleUseCase<DetailBusiness>() {

    private var id: String = ""

    fun setId(id: String) {
        this.id = id
    }

    override fun buildUseCaseSingle(): Single<DetailBusiness> {
        return repository.getDetail(id)
    }
}