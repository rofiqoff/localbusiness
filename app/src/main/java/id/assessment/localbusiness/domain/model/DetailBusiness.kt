package id.assessment.localbusiness.domain.model

data class DetailBusiness(
    var id: String?,
    var alias: String?,
    var name: String?,
    var image_url: String?,
    var is_claimed: String?,
    var is_closed: String?,
    var url: String?,
    var phone: String?,
    var display_phone: String?,
    var review_count: String?,
    var categories: ArrayList<Categories>?,
    var rating: Float?,
    var location: Location?,
    var photos: List<String>?,
    var price: String?,
    var hours: List<Hours>?,
) {
    data class Categories(
        var alias: String?,
        var title: String?
    )

    data class Location(
        var address1: String?,
        var address2: String?,
        var address3: String?,
        var city: String?,
        var zip_code: String?,
        var country: String?,
        var state: String?,
        var display_address: ArrayList<String>?,
        var cross_streets: String?
    )

    data class Hours(
        var open: List<Open>?,
        var is_open_now: Boolean?,
    ) {
        data class Open(
            var is_overnight: Boolean?,
            var start: String?,
            var end: String?,
            var day: Int?,
        )
    }
}