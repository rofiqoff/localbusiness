package id.assessment.localbusiness.domain.model

data class Businesses(
    var businesses: ArrayList<BusinessesData>?,
    var total: Int?
) {
    data class BusinessesData(
        val id: String?,
        val alias: String?,
        val name: String?,
        val image_url: String?,
        val url: String?,
        val review_count: String?,
        val categories: ArrayList<Category>?,
        val rating: Float?,
        val price: String?,
        val location: Location?,
        val phone: String?,
        val display_phone: String?,
        val distance: Double?
    ) {
        data class Category(
            val alias: String?,
            val title: String?
        )

        data class Location(
            val address1: String?,
            val address2: String?,
            val address3: String?,
            val city: String?,
            val zip_code: String?,
            val country: String?,
            val state: String?,
            val display_address: ArrayList<String>?
        )
    }
}
