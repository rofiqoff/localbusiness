package id.assessment.localbusiness.domain.usecase

import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.domain.repository.SearchRepository
import id.assessment.localbusiness.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class SearchUseCase @Inject constructor(
    private val repository: SearchRepository
) : SingleUseCase<Businesses>() {

    private var latitude: String = ""
    private var longitude: String = ""
    private var termToSearch: String = ""

    fun setQuery(latitude: String, longitude: String, termToSearch: String) {
        this.latitude = latitude
        this.longitude = longitude
        this.termToSearch = termToSearch
    }

    override fun buildUseCaseSingle(): Single<Businesses> {
        return repository.search(latitude, longitude, termToSearch)
    }
}