package id.assessment.localbusiness.presentation.detail

import android.view.LayoutInflater
import android.view.MenuItem
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.assessment.localbusiness.databinding.ActivityDetailBinding
import id.assessment.localbusiness.domain.model.DetailBusiness
import id.assessment.localbusiness.presentation.base.BaseActivity
import id.assessment.localbusiness.utils.observe
import javax.inject.Inject

@AndroidEntryPoint
class DetailActivity : BaseActivity<ActivityDetailBinding>() {

    companion object {
        const val ID_EXTRA = "ID_EXTRA"
    }

    @Inject
    lateinit var viewModel: DetailViewModel

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = ActivityDetailBinding::inflate

    override fun initView() {
        initObserver()
        requestDetail()
        setTitleName(" ")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initObserver() {
        observe(viewModel.dataResult, ::showResult)
        observe(viewModel.isLoad, ::handleLoading)
    }

    private fun requestDetail() {
        val id = intent?.getStringExtra(ID_EXTRA) ?: ""
        viewModel.getDetail(id)
    }

    private fun showResult(data: DetailBusiness) {
        binding.data = data

        val image = data.image_url
        val name = data.name

        Glide.with(this).load(image).into(binding.imgBanner)
        setTitleName(name)
    }

    private fun handleLoading(isLoading: Boolean) {
        binding.loading.isVisible = isLoading
        binding.groupViews.isVisible = !isLoading
    }

    private fun setTitleName(title: String?) {
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }
}