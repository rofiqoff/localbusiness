package id.assessment.localbusiness.presentation.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.assessment.localbusiness.databinding.ItemResultBinding
import id.assessment.localbusiness.domain.model.Businesses
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ResultAdapter @Inject constructor(
    private val context: Context
) : RecyclerView.Adapter<ResultAdapter.ViewHolder>() {

    private var data = emptyList<Businesses.BusinessesData>()
    private var onItemClickListener: ((Businesses.BusinessesData) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemResultBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(context, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount(): Int = data.size

    fun updateData(data: List<Businesses.BusinessesData>?) {
        if (data != null) {
            this.data = data
        }
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(listener: ((Businesses.BusinessesData) -> Unit)) {
        this.onItemClickListener = listener
    }

    inner class ViewHolder(private val context: Context, private val binding: ItemResultBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(data: Businesses.BusinessesData) {
            binding.data = data
            binding.executePendingBindings()

            binding.root.setOnClickListener { onItemClickListener?.let { it(data) } }
        }
    }

}