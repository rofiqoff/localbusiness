package id.assessment.localbusiness.presentation.home

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import dagger.hilt.android.AndroidEntryPoint
import id.assessment.localbusiness.databinding.ActivityHomeBinding
import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.presentation.base.BaseActivity
import id.assessment.localbusiness.presentation.detail.DetailActivity
import id.assessment.localbusiness.utils.gone
import id.assessment.localbusiness.utils.hideKeyboard
import id.assessment.localbusiness.utils.observe
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : BaseActivity<ActivityHomeBinding>() {

    companion object {
        private const val REQUEST_CODE_LOCATION = 992
    }

    private var latitude = ""
    private var longitude = ""
    private var term = ""

    @Inject
    lateinit var viewModel: HomeViewModel

    @Inject
    lateinit var adapter: ResultAdapter

    private var locationManager: LocationManager? = null
    private lateinit var locationListener: LocationListener

    override val bindingInflater: (LayoutInflater) -> ViewBinding
        get() = ActivityHomeBinding::inflate

    override fun initView() {
        initObserver()
        initList()
        initLocation()

        binding.btnSearch.setOnClickListener {
            term = binding.edtSearch.text.toString()
            search(term)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGpsPermission()
                }
            }
        }
    }

    private fun initLocation() {
        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = object : LocationListener {
            override fun onLocationChanged(p0: Location) {
                latitude = p0.latitude.toString()
                longitude = p0.longitude.toString()
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

            }

            override fun onProviderDisabled(provider: String) {

            }

            override fun onProviderEnabled(provider: String) {

            }
        }

        checkGpsPermission()
    }

    private fun checkGpsPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
        } else {
            locationManager?.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                0L,
                0F,
                locationListener
            )
        }
    }

    private fun initObserver() {
        observe(viewModel.dataResult, ::showResult)
        observe(viewModel.isLoad, ::handleLoading)
    }

    private fun initList() {
        binding.rvResult.adapter = adapter

        adapter.setOnItemClickListener {
            startActivity(
                Intent(this, DetailActivity::class.java).apply {
                    putExtra(DetailActivity.ID_EXTRA, it.id)
                }
            )
        }
    }

    private fun search(termToSearch: String) {
//        val latitude = "50.08646"
//        val longitude = "14.42155"

        viewModel.search(latitude, longitude, termToSearch)
    }

    private fun showResult(data: Businesses) {
        binding.txtErrorMessage.isVisible = data.businesses?.isEmpty() == true

        val dataSort = data.businesses?.sortedWith(compareBy({ it.distance }, { it.rating }))
        adapter.updateData(dataSort)

        binding.edtSearch.hideKeyboard()
    }

    private fun handleLoading(isLoading: Boolean) {
        if (isLoading) binding.txtErrorMessage.gone()
        binding.loading.isVisible = isLoading
        binding.rvResult.isVisible = !isLoading
    }

}