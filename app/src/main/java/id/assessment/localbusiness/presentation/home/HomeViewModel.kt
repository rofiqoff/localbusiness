package id.assessment.localbusiness.presentation.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.domain.usecase.SearchUseCase
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val searchUseCase: SearchUseCase
) : ViewModel() {

    val dataResult = MutableLiveData<Businesses>()
    val isLoad = MutableLiveData<Boolean>()

    init {
        isLoad.value = false
    }

    fun search(latitude: String, longitude: String, termToSearch: String) {
        isLoad.value = true
        searchUseCase.apply {
            setQuery(latitude, longitude, termToSearch)
            execute(
                onSuccess = {
                    isLoad.value = false
                    dataResult.value = it
                },
                onError = {
                    isLoad.value = false
                    it.printStackTrace()
                })
        }
    }

}