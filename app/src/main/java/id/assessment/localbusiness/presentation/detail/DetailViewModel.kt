package id.assessment.localbusiness.presentation.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.assessment.localbusiness.domain.model.DetailBusiness
import id.assessment.localbusiness.domain.usecase.DetailBusinessUseCase
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val useCase: DetailBusinessUseCase
) : ViewModel() {

    val dataResult = MutableLiveData<DetailBusiness>()
    val isLoad = MutableLiveData<Boolean>()

    init {
        isLoad.value = false
    }

    fun getDetail(id: String) {
        isLoad.value = true
        useCase.apply {
            setId(id)
            execute(
                onSuccess = {
                    isLoad.value = false
                    dataResult.value = it
                },
                onError = {
                    isLoad.value = false
                    it.printStackTrace()
                })
        }
    }
}