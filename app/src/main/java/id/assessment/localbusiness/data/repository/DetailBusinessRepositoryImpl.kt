package id.assessment.localbusiness.data.repository

import id.assessment.localbusiness.data.source.remote.ApiService
import id.assessment.localbusiness.domain.model.DetailBusiness
import id.assessment.localbusiness.domain.repository.DetailBusinessRepository
import io.reactivex.Single

class DetailBusinessRepositoryImpl(
    private val service: ApiService
) : DetailBusinessRepository {
    override fun getDetail(id: String): Single<DetailBusiness> {
        return service.getDetail(id)
    }
}