package id.assessment.localbusiness.data.source.remote

import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.domain.model.DetailBusiness
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("businesses/search")
    fun searchBusinessByLatLon(
        @Query("latitude") latitude: String,
        @Query("longitude") longitude: String,
        @Query("term") termToSearch: String
    ): Single<Businesses>

    @GET("businesses/{id}")
    fun getDetail(@Path("id") id: String): Single<DetailBusiness>

}