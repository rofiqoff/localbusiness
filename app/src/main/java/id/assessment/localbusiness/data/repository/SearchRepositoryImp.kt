package id.assessment.localbusiness.data.repository

import id.assessment.localbusiness.data.source.remote.ApiService
import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.domain.repository.SearchRepository
import io.reactivex.Single

class SearchRepositoryImp(
    private val service: ApiService
) : SearchRepository {
    override fun search(
        latitude: String,
        longitude: String,
        termToSearch: String
    ): Single<Businesses> {
        return service.searchBusinessByLatLon(latitude, longitude, termToSearch)
    }
}