package id.assessment.localbusiness.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.assessment.localbusiness.presentation.home.ResultAdapter
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class AdapterModule {

    @Provides
    @Singleton
    fun provideResultAdapter(@ApplicationContext context: Context): ResultAdapter {
        return ResultAdapter(context)
    }

}