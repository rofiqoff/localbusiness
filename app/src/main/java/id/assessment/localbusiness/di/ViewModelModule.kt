package id.assessment.localbusiness.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.assessment.localbusiness.domain.usecase.DetailBusinessUseCase
import id.assessment.localbusiness.domain.usecase.SearchUseCase
import id.assessment.localbusiness.presentation.detail.DetailViewModel
import id.assessment.localbusiness.presentation.home.HomeViewModel
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ViewModelModule {

    @Provides
    @Singleton
    fun provideHomeViewModel(searchUseCase: SearchUseCase): HomeViewModel {
        return HomeViewModel(searchUseCase)
    }

    @Provides
    @Singleton
    fun provideDetailViewModel(useCase: DetailBusinessUseCase): DetailViewModel {
        return DetailViewModel(useCase)
    }

}