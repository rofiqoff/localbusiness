package id.assessment.localbusiness.utils.adapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import id.assessment.localbusiness.R
import id.assessment.localbusiness.domain.model.Businesses
import id.assessment.localbusiness.domain.model.DetailBusiness
import id.assessment.localbusiness.utils.TimeUtils

object BindingAdapter {

    @BindingAdapter("showImage")
    @JvmStatic
    fun AppCompatImageView.loadImage(url: Any) {
        try {
            Glide.with(context).load(url).into(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @BindingAdapter("showCategoryText")
    @JvmStatic
    fun AppCompatTextView.setCategory(data: ArrayList<Businesses.BusinessesData.Category>?) {
        var category = ""

        data?.forEachIndexed { index, categoryName ->
            if (index >= 1) category += ", "
            category += categoryName.title
        }

        text = category
    }

    @BindingAdapter("showCategoryText")
    @JvmStatic
    fun AppCompatTextView.setCategoryDetail(data: ArrayList<DetailBusiness.Categories>?) {
        var category = ""

        data?.forEachIndexed { index, categoryName ->
            if (index >= 1) category += ", "
            category += categoryName.title
        }

        text = context.getString(R.string.label_category, category)
    }

    @BindingAdapter("showLocationText")
    @JvmStatic
    fun AppCompatTextView.setLocation(data: ArrayList<String>?) {
        var address = ""

        data?.forEachIndexed { index, addressName ->
            if (index >= 1) address += ", "
            address += addressName
        }

        text = address
    }

    @BindingAdapter("showLocationText", "showPhoneNumber")
    @JvmStatic
    fun AppCompatTextView.setLocationInDetail(data: ArrayList<String>?, showPhoneNumber: String?) {
        var address = ""

        data?.forEachIndexed { index, addressName ->
            if (index >= 1) address += ", "
            address += addressName
        }

        text = "${
            context.getString(
                R.string.label_address,
                address
            )
        }\n${context.getString(R.string.label_phone, showPhoneNumber)}"
    }

    @BindingAdapter("showHourOperationText")
    @JvmStatic
    fun AppCompatTextView.showHourOperation(data: List<DetailBusiness.Hours.Open>?) {
        var result = ""

        data?.forEach {
            val day = TimeUtils.convertDay(it.day)
            val startTime = TimeUtils.convertTime(it.start)
            val endTime = TimeUtils.convertTime(it.end)

            result += "$day, $startTime - $endTime\n"
        }

        text = result
    }

}