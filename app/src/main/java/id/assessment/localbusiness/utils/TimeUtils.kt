package id.assessment.localbusiness.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {

    fun convertTime(time: String?): String {
        return try {
            val inputFormat = SimpleDateFormat("HHmm", Locale.getDefault())
            val date = inputFormat.parse(time!!)

            val resultFormat = SimpleDateFormat("K:mm a", Locale.getDefault())
            resultFormat.format(date!!)
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    fun convertDay(day: Int?): String {
        try {
            val days = arrayListOf(
                "Sunday",
                "Monday",
                "Tuesday",
                "Wednesday",
                "Thursday",
                "Friday",
                "Saturday"
            )

            return days[day!!]
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }

}